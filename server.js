const express = require("express");
const path = require("path");
const port = process.env.PORT || 6600;
const app = express();
const cors = require("cors");
const compression = require("compression");


app.use(cors());
app.use(express.json());


app.use(compression());
// the __dirname is the current directory from where the script is running
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, "build")));


app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.listen(port, () => {
  console.log(`listening ${port}`);
});
