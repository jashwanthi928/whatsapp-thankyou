import React, { Fragment } from "react";
import CommunityEnrol from "./Community_enrol";
import CommunityNewEnrol from "./Community_new_enrol";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";


const Navigate = () => {
  return (
    <Router basename="/">
      <Fragment>
              <Switch>
                <Route exact path="/" component={CommunityNewEnrol} />
                <Route exact path="/community-enrol-new" component={CommunityNewEnrol} />
                <Route exact path="/community-enrol" component={CommunityEnrol} />;
              </Switch>
      </Fragment>
    </Router>
  );
};

export default Navigate;

