import React, { useState, useEffect } from "react";
import WhatsappCommunity from "./Images/WhatsappCommunity-01.svg";
import "./App.css";

const CommunityEnrol = (props) => {
  const [ShowFirstScreen, setShowFirstScreen] = useState(true);
  const [ShowSecondScreen, setShowSecondScreen] = useState(false);
  const [ShowThirdScreen, setShowThirdScreen] = useState(false);
  const [SetLink, setSetLink] = useState({ link: "" });
  const [Option, setOption] = useState({ option: "" });
  const [Phone, setPhone] = useState({ phone: "" });

  const handleChangeOption = (e) => {
    setOption({ option: e.target.value });
    if (e.target.value === "Masters Abroad") {
      setSetLink({ link: "http://1.2.vu/apn" });
    } else if (e.target.value === "Placements") {
      setSetLink({ link: "http://1.2.vu/cpn" });
    } else if (e.target.value === "Confused! Yet to decide") {
      setSetLink({ link: "http://1.2.vu/cpn" });
    }
  };
  const handleChangePhone = (e) => {
    setPhone({ phone: e.target.value });
    console.log(Phone.phone);
  };
  const onClickNext = () => {
    if (SetLink.link != "") {
      setTimeout(function () {
        document.getElementById("myBar").style.width = "66.66%";
      }, 100);

      setShowSecondScreen(true);
      setShowFirstScreen(false);
    } else {
      document.getElementById("optionValidation").style.display = "block";
    }
  };
  const onClickNext2 = () => {
    console.log(Option.option);
    setShowThirdScreen(true);
    setShowFirstScreen(false);
    setShowSecondScreen(false);
  };
  const onClickBack = () => {
    setShowFirstScreen(true);
    setShowSecondScreen(false);
    setSetLink({ link: "" });
    document.getElementById("myBar").style.width = "33.33%";
  };
  return (
    <>
      {ShowFirstScreen ? (
        <div id="firstScreen">
          <img className="banner" src={WhatsappCommunity} alt="banner"></img>
          <div className="heading">Build Profile the right way!</div>
          <div className="subHeading">
            Join the right Community to get the Best Support
          </div>
          <hr></hr>

          <div className="QandA">
            <div className="question">
              What is your Focus for Profile Building?
            </div>
            <label>
              <input
                type="radio"
                name="option"
                value="Masters Abroad"
                onChange={handleChangeOption}
              />
              Masters Abroad
            </label>
            <br></br>
            <label>
              <input
                type="radio"
                name="option"
                value="Placements"
                onChange={handleChangeOption}
              />
              Placements
            </label>
            <br></br>
            <label>
              <input
                type="radio"
                name="option"
                value="Confused! Yet to decide"
                onChange={handleChangeOption}
              />
              Confused! Yet to decide
            </label>
            <div className="option3">
              Between Masters & Placements <br></br>
              Between Masters India or Abroad
              <br></br>
              Work for few years and then pursue Masters
            </div>
          </div>
          <div id="optionValidation">Please select one option</div>
          <button id="next1" className="next" onClick={onClickNext}>
            Next
          </button>
          <div id="myProgress">
            <div id="myBar"></div>
          </div>
        </div>
      ) : null}
      {ShowSecondScreen ? (
        <div if="secondScreen">
          <img className="banner" src={WhatsappCommunity} alt="banner"></img>
          <div className="heading">Build Profile the right way!</div>
          <div className="subHeading">
            Join the right Community to get the Best Support
          </div>
          <hr></hr>

          <div className="QandA">
            <div className="share">
              Share your Registered Number for Seminar
            </div>
            <form onSubmit={onClickNext2}>
              <input
                type="tel"
                pattern="[0-9]{10}"
                name="phone"
                placeholder="Phone Number"
                onChange={handleChangePhone}
                required
              />
              <br></br>
              <div className="phoneInfo">10 Digit Registered Number</div>
              <div className="subPhoneInfo">
                even if it's not whatsapp it's fine
              </div>
              <div className="flexCheckbox">
                <input
                  id="regprivacyPolicy"
                  type="checkbox"
                  name="agreePolicy"
                  value="true"
                  checked
                />
                <div className="agreePolicy">
                  I agree for receiving messages from CareerLabs Technologies
                  Private Limited in Whatsapp, calls and SMS* | We hate SPAM
                  too. We will not SPAM.
                </div>
              </div>

              <div className="buttonFlex">
                <button id="back" className="back" onClick={onClickBack}>
                  Back
                </button>
                <input
                  type="submit"
                  value="Next"
                  id="next2"
                  className="next2"
                />
              </div>
            </form>
          </div>

          <div id="myProgress1">
            <div id="myBar"></div>
          </div>
        </div>
      ) : null}
      {ShowThirdScreen ? (
        <div if="thirdScreen">
          <img className="banner" src={WhatsappCommunity} alt="banner"></img>
          <div className="heading">Build Profile the right way!</div>
          <div className="subHeading">
            Join the right Community to get the Best Support
          </div>
          <hr></hr>

          <div className="QandA">
            <div className="share">
              Congrats! You are 1 step away from joining the Profile Building
              for Placement Community
            </div>
            <div class="flexIcons">
              <div class="rating">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
              </div>
              <img
                className="whatsappIcon"
                src="https://cdn2.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2-free/128/social-whatsapp-circle-512.png"
                alt="whatsappIcon"
                width="90"
                height="90"
              ></img>
              <a className="clickJoin" href={SetLink.link} target="_blank">
                CLICK TO JOIN
              </a>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default CommunityEnrol;
